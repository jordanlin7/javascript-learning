'use strict';

const dice = document.querySelector('.dice');
const score0 = document.querySelector('#score--0');
const score1 = document.querySelector('#score--1');
const player0 = document.querySelector('.player--0');
const player1 = document.querySelector('.player--1');
const currentScore0 = document.querySelector('#current--0');
const currentScore1 = document.querySelector('#current--1');
const newgameBtn = document.querySelector('.btn--new');
const rollBtn = document.querySelector('.btn--roll');
const holdBtn = document.querySelector('.btn--hold');

let activePlayer, scores, currentScore, playing;


const init = function () {
    scores = [0, 0];
    activePlayer = 0;
    currentScore = 0;
    playing = true;

    score0.textContent = 0;
    score1.textContent = 0;
    currentScore0.textContent = 0;
    currentScore1.textContent = 0;


    dice.classList.add('hidden');
    player0.classList.add('player--active');
    player1.classList.remove('player--active');
    player0.classList.remove('player--winner');
    player1.classList.remove('player--winner');

}

init();

const changePlayer = function () {
    if (activePlayer === 0) {
        activePlayer = 1;
        player1.classList.add('player--active');
        player0.classList.remove('player--active');
    }
    else {
        activePlayer = 0;
        player0.classList.add('player--active');
        player1.classList.remove('player--active');
    }
}

const saveScoreToPlayer = function () {

    scores[activePlayer] += currentScore;

    console.log(scores);

    score0.textContent = scores[0];
    score1.textContent = scores[1];

    currentScore = 0;
    currentScore0.textContent = 0;
    currentScore1.textContent = 0;    
}



newgameBtn.addEventListener('click', init);


rollBtn.addEventListener('click', function () {
    if (playing) {
        dice.classList.remove('hidden');
        let diceNum = Math.trunc(Math.random() * 6) + 1;

        const dicePic = `dice-${diceNum}.png`;
        dice.src = dicePic;

        if (diceNum !== 1) {
            currentScore += diceNum;

            if (activePlayer === 0) {
                currentScore0.textContent = currentScore;
            }
            else {
                currentScore1.textContent = currentScore;
            }
        }
        else {
            currentScore = 0;
            currentScore0.textContent = 0;
            currentScore1.textContent = 0;
            changePlayer();
        }
    }
})

holdBtn.addEventListener('click', () => {
    if (playing) {
        saveScoreToPlayer();

        if (scores[activePlayer] >= 20) {
            playing = false;

            dice.classList.add('hidden');
      
            document
              .querySelector(`.player--${activePlayer}`)
              .classList.add('player--winner');
            document
              .querySelector(`.player--${activePlayer}`)
              .classList.remove('player--active');
        }
        else {
            changePlayer();            
        }
    }
})
