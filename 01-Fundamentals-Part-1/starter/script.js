// value and variables
let firstName = "Jonas";
console.log(firstName);

// Data types
//////////////////////////////////////
// 7 Primitive data types
//////////////////////////////////////
// 1. Number
// 2. String
// 3. Boolean
// 4. Undefined
// 5. Null
// 6. Symbol (ES2015)
// 7. BigInt (ES2020)


// typeof
let javascriptIsFun = true;
console.log(javascriptIsFun);
console.log(typeof javascriptIsFun); // boolean
console.log(typeof 23);
console.log(typeof 'Jonas');

javascriptIsFun = 100; // 
console.log(typeof javascriptIsFun); // number


////////////////////////////////////////////////
// let, const , var
////////////////////////////////////////////////
let age = 30;
age = 31;
const birthYear = 1991;
//birthYear = 1990; const 無法修改

////////////////////////////////////////////////
// basic operators
////////////////////////////////////////////////
const now = 2037;
const ageJonas = now - 1991;
console.log(ageJonas);
console.log(ageJonas * 2);
console.log(ageJonas / 2, 2 ** 3); // 2 ** 3 = 2 * 2 * 2

const lastName = 'Wahaha';
console.log(firstName + ' ' + lastName);
console.log(`${firstName} ${lastName}`);  // 使用template literal

let x = 10 + 5;
x += 10;    // x = x + 10;
console.log('x=', x);

let y = 10;
console.log(++y);
y = 10;
console.log(y++);


////////////////////////////////////
// Coding Challenge #1

/*
Mark and John are trying to compare their BMI (Body Mass Index), which is calculated using the formula: BMI = mass / height ** 2 = mass / (height * height). (mass in kg and height in meter).

1. Store Mark's and John's mass and height in variables
2. Calculate both their BMIs using the formula (you can even implement both versions)
3. Create a boolean variable 'markHigherBMI' containing information about whether Mark has a higher BMI than John.

TEST DATA 1: Marks weights 78 kg and is 1.69 m tall. John weights 92 kg and is 1.95 m tall.
TEST DATA 2: Marks weights 95 kg and is 1.88 m tall. John weights 85 kg and is 1.76 m tall.

GOOD LUCK 😀
*/

const massMarks = 78;
const heightMarks = 1.69;
const massJohn = 92;
const heightJohn = 1.95;
let bmiMarks = massMarks / heightMarks ** 2;
let bmiJohn = massJohn / heightJohn ** 2;
let markHigherBMI = bmiMarks > bmiJohn;
console.log('bmiMarks', bmiMarks, 'bmiJohn', bmiJohn, 'markHigherBMI', markHigherBMI);

/////////////////////////////////////////////////
// Type Conversion
/////////////////////////////////////////////////
const inputYear = '1991';
console.log(Number(inputYear) + 18);

/////////////////////////////////////////////////
// Type coercion 強制轉型
/////////////////////////////////////////////////
console.log('I am ' + 23 + 'years old'); // 23轉成string
console.log('23' - '10' - 3);
console.log('23' + '10' + 3);
console.log('23' * '2');

let n = '1' + 1; //數字1會轉成string, 加法會把數字變字串
n = n - 1;
console.log(n);
console.log(2 + 3 + 4 - '5');


/////////////////////////////////////////////////
// Truthy and Falsy
/////////////////////////////////////////////////

// 5 falsy values: 0, '', undefined, null, NaN
console.log(Boolean(0));
console.log(Boolean(undefined));
console.log(Boolean('Jonas'));
console.log(Boolean({}));

/////////////////////////////////////////////////
// Equality operators == , ===
/////////////////////////////////////////////////
const myage = 18;
if(myage === 18) {
    console.log('You just became an adult');
}

const myage2 = '18';
if(myage2 == 18) {
    console.log('You just became an adult');
}

////////////////////////////////////
// Coding Challenge #3

/*
There are two gymnastics teams, Dolphins and Koalas. They compete against each other 3 times. The winner with the highest average score wins the a trophy!

1. Calculate the average score for each team, using the test data below
2. Compare the team's average scores to determine the winner of the competition, and print it to the console. Don't forget that there can be a draw, so test for that as well (draw means they have the same average score).

3. BONUS 1: Include a requirement for a minimum score of 100. With this rule, a team only wins if it has a higher score than the other team, and the same time a score of at least 100 points. HINT: Use a logical operator to test for minimum score, as well as multiple else-if blocks 😉
4. BONUS 2: Minimum score also applies to a draw! So a draw only happens when both teams have the same score and both have a score greater or equal 100 points. Otherwise, no team wins the trophy.

TEST DATA: Dolphins score 96, 108 and 89. Koalas score 88, 91 and 110
TEST DATA BONUS 1: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 123
TEST DATA BONUS 2: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 106

GOOD LUCK 😀
*/

const sumDolphins = (96 + 108 + 89);
const sumKoalas = (89 + 91 + 110);

const averageDolphins = sumDolphins / 3;
const averageKoalas =  sumKoalas/ 3;

if(averageDolphins > averageKoalas && averageDolphins >= 100) {
    console.log('Dolphins win');
}
else if(averageDolphins < averageKoalas && averageKoalas >= 100) {
    console.log('Koalas win');
}
else if(averageDolphins === averageKoalas && averageDolphins >= 100 && averageKoalas >= 100) {
    console.log('Draw Game');
}
else {
    console.log('No one wins')
}












