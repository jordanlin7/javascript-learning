'use strict';
// 取出element text
// console.log(document.querySelector('.message').textContent);
// document.querySelector('.message').textContent = 'Correct Number!';

// document.querySelector('.number').textContent = 13;
// document.querySelector('.score').textContent = 10;

// document.querySelector('.guess').value = 23;

let score = 20;
let highScore = 0;

// Create secret Number
const createRandomNumber = function () {
    const minVal = 1;
    const maxVal = 20;
    const randNum = Math.trunc(Math.random() * (maxVal - minVal + 1) + minVal);
    return randNum;
}
let secretNumber = createRandomNumber();
console.log(secretNumber);
//document.querySelector('.number').textContent = secretNumber;





const onClickCheck = function () {

    const guess = Number(document.querySelector('.guess').value);

    if (score > 1) {
        if (!guess) {
            console.log('No number');
        }
        else if (guess === secretNumber) {
            document.querySelector('.message').textContent = 'Correct Number!';
            document.querySelector('body').style.backgroundColor = '#60b347';
            document.querySelector('.number').style.width = '30rem';
            document.querySelector('.number').textContent = secretNumber;
            document.querySelector('.check').disabled = true;

            highScore = highScore < score ? score : highScore;
            document.querySelector('.highscore').textContent = highScore;

        }
        else if (guess > secretNumber) {
            document.querySelector('.message').textContent = 'Too high!';
            score--;
        }
        else if (guess < secretNumber) {
            document.querySelector('.message').textContent = 'Too low!';
            score--;
        }
    }
    else {
        document.querySelector('.message').textContent = 'You lose!';
        score = 0;
    }

    // no input
    document.querySelector('.score').textContent = score;
}

const onClickAgain = function () {

    secretNumber = createRandomNumber();
    //document.querySelector('.number').textContent = secretNumber;

    score = 20;
    document.querySelector('.score').textContent = score;
    document.querySelector('.message').textContent = "Start gurssing...";
    document.querySelector('body').style.backgroundColor = '#222';
    document.querySelector('.guess').value = '';
    document.querySelector('.number').style.width = '15rem';
    document.querySelector('.number').textContent = '?';
    document.querySelector('.check').disabled = false;

}



document.querySelector('.check').addEventListener('click', onClickCheck);
document.querySelector('.again').addEventListener('click', onClickAgain)


