'use strict';

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.close-modal');
const btnOpenModal = document.querySelectorAll('.show-modal');

// classList可以取得使用的class
console.log(modal.classList);



const showModal = function(bShow) {
    if(bShow) {
        modal.classList.remove('hidden');
        overlay.classList.remove('hidden');
    }
    else {
        modal.classList.add('hidden');
        overlay.classList.add('hidden');
    }
}

for(let btn of btnOpenModal) {
    btn.addEventListener('click', () => {
        showModal(true);
    });
}

btnCloseModal.addEventListener('click', function() {
    showModal(false);
})

document.querySelector('.overlay').addEventListener('click', () => {
    showModal(false);
})

document.addEventListener('keydown', function(event) {
    // 检查按下的键是否是ESC键
    if (event.key === 'Escape' || event.key === 'Esc') {        
        showModal(false);
    }
});



